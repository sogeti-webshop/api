package com.petsupplies.api

import com.twitter.finagle.Service
import com.twitter.finagle.http.{Request, Response}
import io.finch.Application

import io.finch.circe._

// Implicit json encoders
import com.petsupplies.implicits.encoders.ProductEncoder.productEncoder
import com.petsupplies.implicits.encoders.ExceptionEncoder.exceptionEncoder
import com.petsupplies.implicits.encoders.UserEncoder.userEncoder
import io.circe.generic.auto._

object Api {
  val service: Service[Request, Response] =
    (UserEndpoint.endpoints :+: ProductEndpoint.endpoints :+: AuthorizationEndpoint.endpoints).toServiceAs[Application.Json]
}
