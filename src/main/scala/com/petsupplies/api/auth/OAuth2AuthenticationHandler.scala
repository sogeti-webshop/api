package com.petsupplies.api.auth

import java.util.Date

import cats.data.OptionT
import com.petsupplies.authenticationservice.AuthService
import com.petsupplies.authenticationservice.interpreter.AuthInterpreter
import com.petsupplies.authenticationservice.types.Client.Client
import com.petsupplies.authenticationservice.types.User.User
import com.petsupplies.authenticationservice.types.{AccessToken => AuthServiceAccessToken}

import scala.concurrent.Future
import scalaoauth2.provider._
import cats.instances.future._
import cats.~>
import com.petsupplies.authenticationservice.dsl.AuthAction

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration

case class OAuth2AuthenticationHandler(authInterpreter: AuthAction ~> Future) extends DataHandler[User] {
  val expirationTime = 3600 //nr of seconds that a token is valid

  def findAuthInfoByAccessToken(accessToken: AccessToken): Future[Option[AuthInfo[User]]] = {
    val program = AuthService.findUserAndClientIdByAccessToken(accessToken.token)
    val result: Future[Option[(User, String)]] = program.foldMap(authInterpreter)

    (for {
      (user, clientId) <- OptionT(result)
    } yield AuthInfo(user, Some(clientId), None, None)).value
  }

  def findAccessToken(token: String): Future[Option[AccessToken]] = {
    (for {
      asAccessToken <- OptionT(AuthService.findAccessToken(token)
          .foldMap(authInterpreter))
    } yield AccessToken(asAccessToken.token, None, None, Some(expirationTime), Date.from(asAccessToken.creationDate))).value
  }

  def validateClient(maybeCredential: Option[ClientCredential], request: AuthorizationRequest): Future[Boolean] = {
    maybeCredential.map(
      credentials => {
        val client = Client(credentials.clientId, credentials.clientSecret)

        AuthService.validateClient(client)
            .foldMap(authInterpreter)
      }
    ).getOrElse(Future.successful(false))
  }

  def findUser(maybeCredential: Option[ClientCredential], request: AuthorizationRequest): Future[Option[User]] = {
    val futureMaybeUser = for {
      email <- request.param("email")
      password <- request.param("password")
    } yield AuthService.findUser(email, password)
        .foldMap(authInterpreter)

    futureMaybeUser.getOrElse(Future.successful(None))
  }

  def createAccessToken(authInfo: AuthInfo[User]): Future[AccessToken] =
    authInfo.clientId.map {
      clientId =>
        AuthService
            .createAccessToken(authInfo.user, clientId)
            .foldMap(authInterpreter)
            .map {
              asAccessToken =>
                AccessToken(asAccessToken.token, None, None, Some(expirationTime), Date.from(asAccessToken.creationDate), Map.empty)
            }
    }.getOrElse(
      Future.failed(
        new Exception("ClientID must be present to create a new access token")
      )
    )

  def getStoredAccessToken(authInfo: AuthInfo[User]): Future[Option[AccessToken]] =
    authInfo.clientId.map(
      clientId =>
        (for {
          AuthServiceAccessToken(user, clientId, token, creationDate) <- OptionT(
            AuthService
                .findAccessTokenByUserAndClientId(authInfo.user, clientId)
                .foldMap(authInterpreter)
          )
        } yield AccessToken(token, None, None, Some(expirationTime), Date.from(creationDate), Map.empty)).value
    ).getOrElse(Future.successful(None))


  // These 4 methods do not need to be implemented for the implicit grant, which is currently the only grant supported
  def refreshAccessToken(authInfo: AuthInfo[User], refreshToken: String): Future[AccessToken] = ???

  def findAuthInfoByCode(code: String): Future[Option[AuthInfo[User]]] = ???

  def deleteAuthCode(code: String): Future[Unit] = ???

  def findAuthInfoByRefreshToken(refreshToken: String): Future[Option[AuthInfo[User]]] = ???
}
