package com.petsupplies.api.auth

import com.twitter.finagle.http._
import com.twitter.util.Future
import com.petsupplies.implicits.extensions.FutureConverter._
import scala.concurrent.ExecutionContext.Implicits.global
import scalaoauth2.provider._

trait OAuth2 {

  private[this] def headersToMap(headers: HeaderMap) = (for {
    key <- headers.keys
  } yield (key, headers.getAll(key))).toMap

  private[this] def paramsToMap(params: ParamMap) = (for {
    key <- params.keys
  } yield (key, params.getAll(key).toSeq)).toMap

  def issueAccessToken[U](request: Request, dataHandler: DataHandler[U]): Future[GrantHandlerResult[U]] =
    OAuth2TokenEndpoint.handleRequest(
      new AuthorizationRequest(headersToMap(request.headerMap), paramsToMap(request.params)),
      dataHandler
    ).asTwitter.flatMap {
      case Left(error) => Future.exception[GrantHandlerResult[U]](error)
      case Right(handler) => Future(handler)
    }

  def authorize[U](request: Request, dataHandler: DataHandler[U]): Future[AuthInfo[U]] =
    ProtectedResource.handleRequest(
      new ProtectedResourceRequest(headersToMap(request.headerMap), paramsToMap(request.params)),
      dataHandler
    ).asTwitter.flatMap {
      case Left(error) => Future.exception[AuthInfo[U]](error)
      case Right(authInfo) => Future(authInfo)
    }
}

object OAuth2 extends OAuth2
