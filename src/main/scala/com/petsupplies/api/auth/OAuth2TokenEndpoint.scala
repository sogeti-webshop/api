package com.petsupplies.api.auth

import scalaoauth2.provider.{GrantHandler, Implicit, TokenEndpoint}

object OAuth2TokenEndpoint extends TokenEndpoint {
  override val handlers: Map[String, GrantHandler] =
    Map.apply(
      "implicit" -> new Implicit()
    )
}
