package com.petsupplies.api.auth.utils

import com.twitter.finagle.http.{Response, Status, Version}

import scalaoauth2.provider.OAuthError

object OAuthErrorExtension {

  implicit class RichOAuthError(val oauthError: OAuthError) extends AnyVal {
    def toHttpResponse: Response = {
      val bearer = Seq("error=\"" + oauthError.errorType + "\"") ++
          (if (!oauthError.description.isEmpty) Seq("error_description=\"" + oauthError.description + "\"") else Nil)

      val rep = Response(Version.Http11, Status(oauthError.statusCode))
      rep.headerMap.add("WWW-Authenticate", "Bearer " + bearer.mkString(", "))

      rep
    }
  }

}

