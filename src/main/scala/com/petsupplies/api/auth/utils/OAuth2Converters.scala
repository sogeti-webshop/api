package com.petsupplies.api.auth.utils

import com.petsupplies.authenticationservice.types.User.User
import com.twitter.finagle.http.Response

import scala.util.parsing.json.JSONObject
import scalaoauth2.provider.{GrantHandlerResult, OAuthError}
import com.petsupplies.api.auth.utils.OAuthErrorExtension._

object OAuth2Converters {

  trait OAuthErrorHandler {
    def handleError(e: OAuthError): Response
  }

  trait OAuthErrorInJson extends OAuthErrorHandler {
    override def handleError(e: OAuthError): Response = {
      val rep = e.toHttpResponse
      val json = Map(
        "status" -> e.statusCode,
        "error" -> e.errorType,
        "description" -> e.description
      )

      rep.setContentTypeJson()
      rep.setContentString(JSONObject(json).toString())

      rep
    }
  }

  trait OAuthTokenConverter[U] {
    def convertToken(token: GrantHandlerResult[U]): Response
  }

  trait OAuthTokenInJson extends OAuthTokenConverter[User] {
    override def convertToken(token: GrantHandlerResult[User]): Response = {
      val rep = Response()
      val json = Map[String, Any](
        "access_token" -> token.accessToken,
        "token_type" -> token.tokenType
      ) ++ token.expiresIn.map(
        "expires_in" -> _
      ) ++ token.refreshToken.map(
        "refresh_token" -> _
      ) ++ token.scope.map(
        "scope" -> _
      )

      rep.setContentTypeJson()
      rep.setContentString(JSONObject(json).toString())

      rep
    }
  }
}
