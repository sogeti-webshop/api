package com.petsupplies.api

import cats.data.EitherT
import com.petsupplies.authenticationservice.AuthService
import com.petsupplies.authenticationservice.dsl.Auth.AuthF
import com.petsupplies.authenticationservice.interpreter.AuthInterpreter
import com.petsupplies.authenticationservice.util.validation.ValidationFailed
import com.petsupplies.authenticationservice.types.User.{User, hashPassword, validate}
import io.finch.{BadRequest, Created, Endpoint, Ok, Output, get, jsonBody, post}
import com.petsupplies.implicits.decoders.UserDecoder._
import io.finch.circe._
import com.petsupplies.authenticationservice.util.validation.semigroup.ValidationErrorSemigroup._
import com.twitter.util.{Future => TwitterFuture}
import com.petsupplies.api.AuthorizationEndpoint.defaultDataHandler
import io.catbird.util._
import com.petsupplies.authenticationservice.implicits._
import scalaoauth2.provider.{AuthInfo, DataHandler}

object UserEndpoint {
  private val resourceName = "user"

  val register: Endpoint[AuthF[Either[Throwable, Unit]]] = post(resourceName :: jsonBody[User]) {
    user: User => {
      validate(user).map(hashPassword)
          .leftMap(_.reduce(validationErrorSemigroup))
          .fold(
            {
              case ValidationFailed(message) => BadRequest {
                new Exception(message)
              }
            },
            user => Created {
              AuthService.register(user)
            }
          )
    }
  }

  def getUser(implicit dataHandler: DataHandler[User]): Endpoint[User] =
    get(resourceName :: AuthorizationEndpoint.protectEndpoint(dataHandler)) {
      authInfo: AuthInfo[User] =>
        Ok {
          authInfo.user
        }
    }

  def mapRegisterResultToOutput[T](result: TwitterFuture[Either[Throwable, Unit]]): TwitterFuture[Output[Unit]] =
    EitherT(result).fold(
      _ => BadRequest {
        new Exception("Het opgegeven e-mailadres is al in gebruik")
      },
      _ => Created {}
    )

  private[this] def interpret[A](program: AuthF[A]): TwitterFuture[A] =
    program.foldMap[TwitterFuture](AuthInterpreter.interpret[TwitterFuture])

  val endpoints = {
    register.mapOutputAsync(x => mapRegisterResultToOutput(interpret(x))) :+: getUser
  }
}
