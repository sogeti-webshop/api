package com.petsupplies.api

import com.petsupplies.productservice.ProductService
import com.petsupplies.productservice.persistence.ProductRepository.doobieProductRepository
import com.petsupplies.productservice.persistence.ProductQuery.productQuery
import com.petsupplies.productservice.persistence.Transactor._
import com.petsupplies.productservice.dsl.Product.ProductF
import com.petsupplies.productservice.interpreter.ProductInterpreter
import com.petsupplies.productservice.types.Product.Product
import io.finch.{Endpoint, Ok, get}
import io.finch._
import com.twitter.util.Future
import io.catbird.util._

object ProductEndpoint {

  val getProducts: Endpoint[ProductF[List[Product]]] = get("product" :: "all") {
    Ok {
      ProductService.all
    }
  }

  private[this] def interpret[A](program: ProductF[A]) = program.foldMap(ProductInterpreter.interpret[Future])

  lazy val endpoints: Endpoint[List[Product]] = getProducts.mapAsync(interpret)
}
