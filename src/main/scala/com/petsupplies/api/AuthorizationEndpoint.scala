package com.petsupplies.api

import com.petsupplies.api.auth.OAuth2AuthenticationHandler
import com.petsupplies.authenticationservice.types.User.User
import com.petsupplies.api.auth.OAuth2FinchProvider._
import com.petsupplies.authenticationservice.AuthService
import com.petsupplies.authenticationservice.dsl.Auth.AuthF
import io.finch.{Endpoint, post}
import io.finch._
import com.petsupplies.authenticationservice.interpreter.AuthInterpreter
import scalaoauth2.provider.{AuthInfo, DataHandler, GrantHandlerResult}
import com.twitter.util.{Future => TwitterFuture}
import cats.instances.future._
import scala.concurrent.ExecutionContext.Implicits.global
import com.petsupplies.authenticationservice.implicits._
import io.catbird.util._
import scala.concurrent.{ Future => ScalaFuture }


object AuthorizationEndpoint {
  private val resourceName = "auth"

  implicit val defaultDataHandler: DataHandler[User] = OAuth2AuthenticationHandler(AuthInterpreter.interpret[ScalaFuture])

  def protectEndpoint(dataHandler: DataHandler[User]): Endpoint[AuthInfo[User]] = authorize(dataHandler)

  def token(implicit dataHandler: DataHandler[User]): Endpoint[GrantHandlerResult[User]] =
    post(resourceName :: issueAccessToken(dataHandler))

  def logout(implicit dataHandler: DataHandler[User]): Endpoint[AuthF[Unit]] =
    delete(resourceName :: AuthorizationEndpoint.protectEndpoint(dataHandler)) {
      authInfo: AuthInfo[User] =>
        authInfo.clientId.map {
          clientId =>
            Ok {
              AuthService.logout(authInfo.user.personalData.email, clientId)
            }
        }.getOrElse(
          BadRequest {
            new Exception("Client Id is not provided")
          }
        )
    }

  private[this] def interpret[A](program: AuthF[A]): TwitterFuture[A] =
    program.foldMap[TwitterFuture](AuthInterpreter.interpret[TwitterFuture])

  val endpoints = token :+: logout.mapAsync(interpret)
}
