package com.petsupplies

import com.petsupplies.Config.{corsPolicy, port}
import com.petsupplies.api.Api
import com.twitter.finagle.Http
import com.twitter.finagle.http.filter.Cors.HttpFilter
import com.twitter.util.Await

object Main extends App {
  val httpFilter: HttpFilter = new HttpFilter(corsPolicy)

  Await.result(Http.server.serve(":" + port.toString, httpFilter andThen Api.service))
}
