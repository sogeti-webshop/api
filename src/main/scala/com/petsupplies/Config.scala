package com.petsupplies

import com.twitter.finagle.http.filter.Cors.Policy

object Config {
  val port = 8081

  // Usually this config should live in a config file like yml. Sadly enough there is no purely functional parser
  // This can be created if there's time left. For now, we keep it like this
  val corsPolicy: Policy = Policy(
    allowsOrigin = _ => Some("*"),
    allowsMethods = _ => Some(Seq("GET", "POST", "PUT", "DELETE")),
    allowsHeaders = _ => Some(Seq("content-type", "accept"))
  )
}
