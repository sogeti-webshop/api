package com.petsupplies.implicits.decoders

import com.petsupplies.authenticationservice.types.PersonalData.PersonalData
import com.petsupplies.authenticationservice.types.User.User
import io.circe.Decoder

object UserDecoder {
  implicit val personalDataDecoder: Decoder[PersonalData] = Decoder.forProduct5[String, String, String, String, String, PersonalData](
    "email",
    "fullName",
    "address",
    "postalCode",
    "residence"
  )(PersonalData.apply)

  implicit val userDecoder: Decoder[User] = Decoder.forProduct2[PersonalData, String, User](
    "personalData",
    "password"
  )(User.apply)
}
