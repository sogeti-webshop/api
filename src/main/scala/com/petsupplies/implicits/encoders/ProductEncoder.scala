package com.petsupplies.implicits.encoders

import com.petsupplies.productservice.types.Product.Product
import io.circe.{Encoder, Json}

object ProductEncoder {
  implicit val productEncoder: Encoder[Product] = (product: Product) => Json.obj(
    "id" -> Json.fromInt(product.id),
    "name" -> Json.fromString(product.name),
    "description" -> Json.fromString(product.description),
    "price" -> Json.fromBigDecimal(product.price)
  )
}
