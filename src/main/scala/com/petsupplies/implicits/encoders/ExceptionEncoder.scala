package com.petsupplies.implicits.encoders

import io.circe.{Encoder, Json}

import scalaoauth2.provider.OAuthError

object ExceptionEncoder {
  implicit val exceptionEncoder: Encoder[Exception] = (exception: Exception) => {
    val message = exception match {
      case error: OAuthError => error.description
      case _ => exception.getMessage
    }

    Json.obj(
      "message" -> Json.fromString(message)
    )
  }
}
