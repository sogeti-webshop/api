package com.petsupplies.implicits.encoders

import com.petsupplies.authenticationservice.types.User.User
import io.circe.{Encoder, Json}

object UserEncoder {
  implicit val userEncoder: Encoder[User] =
    user =>
      Json.obj(
        "personalData" -> Json.obj(
          "email" -> Json.fromString(user.personalData.email),
          "fullName" -> Json.fromString(user.personalData.fullName),
          "address" -> Json.fromString(user.personalData.address),
          "postalCode" -> Json.fromString(user.personalData.postalCode),
          "residence" -> Json.fromString(user.personalData.residence)
        )
      )
  

  implicit val userWithPasswordEncoder: Encoder[User] =
    user =>
      Json.obj(
        "personalData" -> Json.obj(
          "email" -> Json.fromString(user.personalData.email),
          "fullName" -> Json.fromString(user.personalData.fullName),
          "address" -> Json.fromString(user.personalData.address),
          "postalCode" -> Json.fromString(user.personalData.postalCode),
          "residence" -> Json.fromString(user.personalData.residence)
        ),
        "password" -> Json.fromString(user.password),
      )
}
