package com.petsupplies.implicits

import cats.Functor
import com.twitter.util.Future

object FunctorInstances {
  implicit def twitterFutureFunctor: Functor[Future] = new Functor[Future] {
    def map[A, B](fa: Future[A])(f: (A) => B): Future[B] = fa.map(f)
  }
}
