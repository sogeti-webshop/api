package com.petsupplies

import org.scalatest.{FlatSpec, Matchers}

class ConfigSpec extends FlatSpec with Matchers {
  "Finagle server" should "run on port 8081" in {
    Config.port should be(8081)
  }

  // The arguments to the functions of cors policies will be ignored and the actual values get returned
  "Cors filter" should "allow all origins" in {
    Config.corsPolicy.allowsOrigin("").get should be("*")
  }

  "Cors filter" should "allow only accept header" in {
    Config.corsPolicy.allowsHeaders(List("")).get should be(List("content-type", "accept"))
  }

  "Cors filter" should "accept GET, POST, PUT, DELETE" in {
    Config.corsPolicy.allowsMethods("").get should be(List("GET", "POST", "PUT", "DELETE"))
  }

}
