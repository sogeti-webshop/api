package com.petsupplies.api

import com.petsupplies.interpreters.PureIdAuthInterpreter
import com.petsupplies.authenticationservice.types.User.User
import io.finch.{Application, Input}
import org.scalatest.Matchers
import com.petsupplies.implicits.encoders.UserEncoder.userWithPasswordEncoder
import io.finch.circe._
import org.scalacheck.Properties
import org.scalacheck.Prop.forAll
import com.petsupplies.generators.UserGen._
import com.twitter.finagle.http.Status

object UserEndpointProp extends Properties("User endpoint") with Matchers {
  property("valid users should be able to register when posting to user resource") = forAll(validUserGen) {
    user: User => {
      val endpoint = UserEndpoint.register(Input.post("/user").withBody[Application.Json](user))

      endpoint.awaitOutputUnsafe() match {
        case Some(output) => {
          assert(output.status === Status.Created)

          output.value.foldMap(PureIdAuthInterpreter.interpret).isRight
        }
        case None => {
          fail()
        }
      }
    }
  }
}
