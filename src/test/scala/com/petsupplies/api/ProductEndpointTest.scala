package com.petsupplies.api

import com.petsupplies.interpreters.PureProductInterpreter
import io.finch.Input
import org.scalatest.{FlatSpec, Matchers}

class ProductEndpointTest extends FlatSpec with Matchers {
  "getProducts endpoint" should "retrieve all products" in {
    ProductEndpoint.getProducts(Input.get("/product/all")).awaitValueUnsafe() match {
      case Some(program) =>
        program.foldMap(PureProductInterpreter.pureInterpret) should be(PureProductInterpreter.products)
      case None =>
        fail()
    }
  }
}
