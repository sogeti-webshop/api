package com.petsupplies.api.auth

import java.util.Date

import com.petsupplies.authenticationservice.types.PersonalData.PersonalData
import com.petsupplies.authenticationservice.types.User.User

import scala.concurrent.Future
import scalaoauth2.provider._

object FakeOAuth2AuthenticationHandler extends DataHandler[User] {
  def findAuthInfoByAccessToken(accessToken: AccessToken): Future[Option[AuthInfo[User]]] = {
    val clientId = if (accessToken.token == "C834750C0E9BC9F5CE5423F83331EA478C09347D42C7698116561BA5B204A4AF") {
      None
    } else {
      Some("df475f81-29fa-4046-b931-379f8c941f77")
    }

    Future.successful(
      Some(
        AuthInfo(
          User(
            PersonalData("email@hotmail.com", "full name", "address", "1102 XL", "Amsterdam"),
            "password"
          ),
          clientId,
          None,
          None
        )
      )
    )
  }

  def findAccessToken(token: String): Future[Option[AccessToken]] =
    Future.successful(
      Some(
        AccessToken(
          token,
          None,
          None,
          None,
          new Date(1501675737)
        )
      )
    )

  def validateClient(maybeCredential: Option[ClientCredential], request: AuthorizationRequest): Future[Boolean] =
    Future.successful(true)

  def findUser(maybeCredential: Option[ClientCredential], request: AuthorizationRequest): Future[Option[User]] =
    Future.successful(
      Some(
        User(
          PersonalData("email@hotmail.com", "full name", "address", "1102 XL", "Amsterdam"),
          "password"
        )
      )
    )

  def createAccessToken(authInfo: AuthInfo[User]): Future[AccessToken] =
    Future.successful(
      AccessToken(
        "6BBB0DA1891646E58EB3E6A63AF3A6FC3C8EB5A0D44824CBA581D2E14A0450CF",
        None,
        None,
        None,
        new Date(1501675737)
      )
    )

  def getStoredAccessToken(authInfo: AuthInfo[User]): Future[Option[AccessToken]] =
    Future.successful(
      Some(
        AccessToken(
          "6BBB0DA1891646E58EB3E6A63AF3A6FC3C8EB5A0D44824CBA581D2E14A0450CF",
          None,
          None,
          None,
          new Date(1501675737)
        )
      )
    )

  def refreshAccessToken(authInfo: AuthInfo[User], refreshToken: String): Future[AccessToken] = ???

  def findAuthInfoByCode(code: String): Future[Option[AuthInfo[User]]] = ???

  def deleteAuthCode(code: String): Future[Unit] = ???

  def findAuthInfoByRefreshToken(refreshToken: String): Future[Option[AuthInfo[User]]] = ???
}
