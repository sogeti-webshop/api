package com.petsupplies.api.auth

import java.util.Date

import com.petsupplies.interpreters.AuthInterpreterTestData._
import com.petsupplies.authenticationservice.types.User.User
import com.petsupplies.interpreters.FutureAuthInterpreter
import org.scalatest.{FlatSpec, Matchers}

import scala.collection.immutable.HashMap
import scalaoauth2.provider._
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.util.{Failure, Success, Try}

class OAuthAuthenticationHandlerSpec extends FlatSpec with Matchers {
  val authenticationHandler: OAuth2AuthenticationHandler = OAuth2AuthenticationHandler(FutureAuthInterpreter.interpret)
  val dummyAuthorizationRequest: AuthorizationRequest = new AuthorizationRequest(new HashMap(), new HashMap())

  "findAuthInfoByAccessToken" should "get auth info for the provided access token" in {
    val accessToken = AccessToken(
      dummyCorrectAccessToken,
      None,
      None,
      Some(3600),
      new Date(1501591800)
    )

    // Futures are already successfull, so these result calls won't consume additional time in test
    val result: Option[AuthInfo[User]] =
      Await.result(authenticationHandler.findAuthInfoByAccessToken(accessToken), Duration.Inf)

    result match {
      case Some(authInfo) =>
        assert(
          authInfo == AuthInfo(
            dummyUser,
            Some(dummyCorrectClientId),
            None,
            None
          )
        )

      case None => fail()
    }
  }

  "findAuthInfoByAccessToken" should "result in None if not found" in {
    val accessToken = AccessToken(
      "3a3b3e0ae57ad4a7ef658c1f7832774f55e403f01fdf44b68b355ec4587d7a04",
      None,
      None,
      Some(3600),
      new Date(1501591800),
      new HashMap()
    )

    val result: Option[AuthInfo[User]] =
      Await.result(authenticationHandler.findAuthInfoByAccessToken(accessToken), Duration.Inf)

    assert(result.isEmpty)
  }

  "findAccessToken" should "return the complete access token" in {
    val stringAccessToken = dummyCorrectAccessToken

    val result: Option[AccessToken] =
      Await.result(authenticationHandler.findAccessToken(stringAccessToken), Duration.Inf)

    result match {
      case Some(accessToken) => {
        val actual = accessToken
        val expected = AccessToken(
          dummyCorrectAccessToken,
          None,
          None,
          Some(3600),
          Date.from(dummyInstant)
        )

        assert(actual == expected)
      }

      case None => fail()
    }
  }

  "findAccessToken" should "return none if not found" in {
    val stringAccessToken = "3a3b3e0ae57ad4a7ef658c1f7832774f55e403f01fdf44b68b355ec4587d7a04"

    val result: Option[AccessToken] =
      Await.result(authenticationHandler.findAccessToken(stringAccessToken), Duration.Inf)

    assert(result.isEmpty)
  }

  "validateClient" should "return true if client is valid" in {
    val clientCredentials = Some(ClientCredential(dummyCorrectClientId, None))

    val isValidClient =
      Await.result(
        authenticationHandler.validateClient(
          clientCredentials,
          dummyAuthorizationRequest
        ),
        Duration.Inf
      )

    assert(isValidClient)
  }

  "validateClient" should "return successful future of false when client is not found" in {
    val clientCredentials = Some(ClientCredential("4783c4ad-e559-44dd-a52f-e7a6b1f54f9c", None))

    val isValidClient =
      Await.result(
        authenticationHandler.validateClient(
          clientCredentials,
          dummyAuthorizationRequest
        ),
        Duration.Inf
      )

    assert(!isValidClient)
  }

  "validateClient" should "return successful future of false when no credentials are provided" in {

    val isValidClient =
      Await.result(authenticationHandler.validateClient(None, dummyAuthorizationRequest), Duration.Inf)

    assert(!isValidClient)
  }

  "findUser" should "return the user if exist" in {
    val clientCredentials = Some(ClientCredential("4783c4ad-e559-44dd-a52f-e7a6b1f54f9c", None))
    val authorizationRequest = new AuthorizationRequest(
      HashMap.empty,
      HashMap.empty +
          (("email", Seq("email@hotmail.com"))) +
          (("password", Seq("5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8")))
    )

    val maybeUser =
      Await.result(
        authenticationHandler.findUser(
          clientCredentials,
          authorizationRequest
        ),
        Duration.Inf
      )

    maybeUser match {
      case Some(user) =>
        assert(user == dummyUser)
      case None => fail()
    }
  }

  "findUser" should "return successful future of none if combination of email and password is not found" in {
    val clientCredentials = Some(ClientCredential("4783c4ad-e559-44dd-a52f-e7a6b1f54f9c", None))
    val authorizationRequest = new AuthorizationRequest(
      HashMap.empty,
      HashMap.empty + (("email", Seq("email@hotmail.com"))) + (("password", Seq("incorrect password")))
    )

    val maybeUser =
      Await.result(
        authenticationHandler.findUser(
          clientCredentials,
          authorizationRequest
        ),
        Duration.Inf
      )

    assert(maybeUser.isEmpty)
  }

  "findUser" should "return successful future of none if password is not found" in {
    val clientCredentials = Some(ClientCredential("4783c4ad-e559-44dd-a52f-e7a6b1f54f9c", None))
    val authorizationRequest = new AuthorizationRequest(
      HashMap.empty,
      HashMap.empty + (("email", Seq("email@hotmail.com")))
    )

    val maybeUser =
      Await.result(
        authenticationHandler.findUser(
          clientCredentials,
          authorizationRequest
        ),
        Duration.Inf
      )

    assert(maybeUser.isEmpty)
  }

  "createAccessToken" should "create new access token if clientId is available" in {
    val authInfo: AuthInfo[User] = AuthInfo(
      dummyUser,
      Some(dummyCorrectClientId),
      None,
      None
    )
    val result =
      Await.result(authenticationHandler.createAccessToken(authInfo), Duration.Inf)

    assert(
      result == AccessToken(
        dummyCorrectAccessToken,
        None,
        None,
        Some(3600),
        new Date(1501591800)
      )
    )
  }

  "createAccessToken" should "result in failed future if clientId is not present" in {
    val authInfo: AuthInfo[User] = AuthInfo(
      dummyUser,
      None,
      None,
      None
    )
    val result =
      Try(Await.result(authenticationHandler.createAccessToken(authInfo), Duration.Inf))

    result match {
      case Success(_) => fail()
      case Failure(exception) =>
        assert(
          exception.getMessage == "ClientID must be present to create a new access token"
        )
    }
  }

  "getStoredAccessToken" should "return the access token when present" in {
    val authInfo: AuthInfo[User] = AuthInfo(
      dummyUser,
      Some(dummyCorrectClientId),
      None,
      None
    )
    val result =
      Await.result(authenticationHandler.getStoredAccessToken(authInfo), Duration.Inf)

    result match {
      case Some(accessToken) =>
        assert(
          accessToken == AccessToken(
            dummyCorrectAccessToken,
            None,
            None,
            Some(3600),
            new Date(1501591800)
          )
        )
      case None => fail()
    }
  }

  "getStoredAccessToken" should "return successful future with none when clientId is not present" in {
    val authInfo: AuthInfo[User] = AuthInfo(
      dummyUser,
      None,
      None,
      None
    )

    val result =
      Await.result(authenticationHandler.getStoredAccessToken(authInfo), Duration.Inf)

    assert(result.isEmpty)
  }

  "getStoredAccessToken" should "return successful future when combination user and clientId is not found" in {
    val authInfo: AuthInfo[User] = AuthInfo(
      dummyUser,
      Some("Some different client id"),
      None,
      None
    )

    val result =
      Await.result(authenticationHandler.getStoredAccessToken(authInfo), Duration.Inf)

    assert(result.isEmpty)
  }
}
