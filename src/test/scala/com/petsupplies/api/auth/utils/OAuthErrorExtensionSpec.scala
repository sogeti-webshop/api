package com.petsupplies.api.auth.utils

import scalaoauth2.provider.InvalidRequest
import com.petsupplies.api.auth.utils.OAuthErrorExtension._
import com.twitter.finagle.http.Version
import org.scalatest.{FlatSpec, Matchers}

class OAuthErrorExtensionSpec extends FlatSpec with Matchers {

  "OAuthError" should "covert to correct HTTP Response" in {
    val oauthError = new InvalidRequest("error description")

    val response = oauthError.toHttpResponse

    assert(response.version == Version.Http11)
    assert(response.headerMap.get("WWW-Authenticate").get == "Bearer error=\"invalid_request\", error_description=\"error description\"")
  }

  "OAuthError" should "omit the error description if description is empty" in {
    val oauthError = new InvalidRequest()

    val response = oauthError.toHttpResponse

    assert(response.version == Version.Http11)
    assert(response.headerMap.get("WWW-Authenticate").get == "Bearer error=\"invalid_request\"")
  }
}
