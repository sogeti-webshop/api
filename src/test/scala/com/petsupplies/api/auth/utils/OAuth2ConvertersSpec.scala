package com.petsupplies.api.auth.utils

import com.petsupplies.api.auth.utils.OAuth2Converters.{OAuthErrorInJson, OAuthTokenInJson}
import com.petsupplies.authenticationservice.types.PersonalData.PersonalData
import com.petsupplies.authenticationservice.types.User.User
import org.scalatest.{FlatSpec, Matchers}

import scala.util.parsing.json.JSONObject
import scalaoauth2.provider.{AuthInfo, GrantHandlerResult, InvalidRequest}

class OAuth2ConvertersSpec extends FlatSpec with Matchers {
  "OAuthError" should "be convertable to json http response" in {
    val oauthErrorToJson = new OAuthErrorInJson {}

    val response = oauthErrorToJson.handleError(new InvalidRequest("error description"))

    val json = Map(
      "status" -> 400,
      "error" -> "invalid_request",
      "description" -> "error description"
    )

    response.setContentTypeJson()
    assert(response.getContentString() == JSONObject(json).toString())
  }

  "OAuthToken" should "be convertable to json http response" in {
    val oauthTokenToJson = new OAuthTokenInJson {}

    val grantHandlerResult = new GrantHandlerResult[User](
      AuthInfo(
        User(
          PersonalData("email@hotmail.com", "full name", "address", "2293 XL", "Amsterdam"),
          "password"
        ),
        Some("d91a633f-13d0-4f1c-890c-05e89bfe3284"),
        None,
        None
      ),
      "bearer",
      "1D69BD02677099AEA1A47BD3E31A94FEE7D38FE50F838A82AF94BC67CE3234EE",
      None,
      None,
      None,
      Map()
    )

    val json = Map(
      "access_token" -> "1D69BD02677099AEA1A47BD3E31A94FEE7D38FE50F838A82AF94BC67CE3234EE",
      "token_type" -> "bearer"
    )

    val response = oauthTokenToJson.convertToken(grantHandlerResult)

    response.setContentTypeJson()
    assert(response.getContentString() == JSONObject(json).toString())
  }

  "OAuth2Token" should "add additional fields to json http response if present" in {
    val oauthTokenToJson = new OAuthTokenInJson {}

    val grantHandlerResult = new GrantHandlerResult[User](
      AuthInfo(
        User(
          PersonalData("email@hotmail.com", "full name", "address", "2293 XL", "Amsterdam"),
          "password"
        ),
        Some("d91a633f-13d0-4f1c-890c-05e89bfe3284"),
        None,
        None
      ),
      "bearer",
      "1D69BD02677099AEA1A47BD3E31A94FEE7D38FE50F838A82AF94BC67CE3234EE",
      Some(3600),
      Some("9F81E14BE56F14F2CEAEC2DCE1880CD8432AD85289E350CCCB97FD54FC3FE919"),
      Some("all"),
      Map()
    )

    val json = Map(
      "access_token" -> "1D69BD02677099AEA1A47BD3E31A94FEE7D38FE50F838A82AF94BC67CE3234EE",
      "token_type" -> "bearer",
      "expires_in" -> 3600,
      "refresh_token" -> "9F81E14BE56F14F2CEAEC2DCE1880CD8432AD85289E350CCCB97FD54FC3FE919",
      "scope" -> "all"
    )

    val response = oauthTokenToJson.convertToken(grantHandlerResult)

    response.setContentTypeJson()
    assert(response.getContentString() == JSONObject(json).toString())
  }

}
