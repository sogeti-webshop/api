package com.petsupplies.api

import com.petsupplies.api.auth.FakeOAuth2AuthenticationHandler
import com.petsupplies.authenticationservice.types.User.User
import io.finch.Input
import org.scalatest.{FlatSpec, Matchers}

import scalaoauth2.provider.AuthInfo
import cats.instances.future._
import com.petsupplies.authenticationservice.types.PersonalData.PersonalData

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration

class AuthorizationEndpointSpec extends FlatSpec with Matchers {
  "Post request to token endpoint" should "result in token if client and user is valid" in {
    val input = constructInputForTokenEndpoint(
      "/auth",
      "implicit",
      "df475f81-29fa-4046-b931-379f8c941f77",
      "example@hotmail.com",
      "password"
    )

    val endpoint = AuthorizationEndpoint.token(FakeOAuth2AuthenticationHandler)
    val result = endpoint(input).awaitOutputUnsafe().get.value

    assert(
      result.authInfo == AuthInfo(
        User(
          PersonalData("email@hotmail.com", "full name", "address", "1102 XL", "Amsterdam"),
          "password"
        ),
        Some("df475f81-29fa-4046-b931-379f8c941f77"),
        None,
        None
      )
    )

    assert(result.tokenType == "Bearer")

    assert(result.accessToken == "6BBB0DA1891646E58EB3E6A63AF3A6FC3C8EB5A0D44824CBA581D2E14A0450CF")
  }

  "Delete request to auth endpoint" should "logout the authenticated user" in {
    val input = Input.delete("/auth?access_token=6BBB0DA1891646E58EB3E6A63AF3A6FC3C8EB5A0D44824CBA581D2E14A0450CF")

    val endpoint = AuthorizationEndpoint.logout(FakeOAuth2AuthenticationHandler)
    val output = endpoint(input).awaitOutputUnsafe().get

    assert(output.status.code == 200)
  }

  "Delete request to auth endpoint" should "send bad request if the client id is not present" in {
    val input = Input.delete("/auth?access_token=C834750C0E9BC9F5CE5423F83331EA478C09347D42C7698116561BA5B204A4AF")

    val endpoint = AuthorizationEndpoint.logout(FakeOAuth2AuthenticationHandler)
    val output = endpoint(input).awaitOutputUnsafe().get

    assert(output.status.code == 400)
  }

  private[this] def constructInputForTokenEndpoint(
    path: String,
    grantType: String,
    clientId: String,
    email: String,
    password: String
  ): Input = {
    Input.post(s"$path?grant_type=$grantType&client_id=$clientId&email=$email&password=$password")
  }
}
