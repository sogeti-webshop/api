package com.petsupplies.api

import com.petsupplies.api.auth.FakeOAuth2AuthenticationHandler
import com.petsupplies.authenticationservice.types.PersonalData.PersonalData
import com.petsupplies.authenticationservice.types.User.User
import com.twitter.finagle.http.Status
import com.twitter.util.{Await, Future}
import io.finch.{Application, Input}
import org.scalatest.{FlatSpec, Matchers}
import scala.util.{Failure, Success, Try}
import io.finch.circe._
import io.finch.Output.Payload
import com.petsupplies.implicits.encoders.UserEncoder.userWithPasswordEncoder

class UserEndpointSpec extends FlatSpec with Matchers {
  "UserEndpoint" should "respond with bad request with all validation errors when user is invalid" in {
    val user = User(
      PersonalData("email@hotmail.com", "", "address 12", "non valid postal code", "residence"),
      "password"
    )

    val endpoint = UserEndpoint.register(Input.post("/user").withBody[Application.Json](user))

    endpoint.awaitOutputUnsafe() match {
      case Some(output) =>
        assert(output.status === Status.BadRequest)

        Try(output.value) match {
          case Success(_) => fail()
          case Failure(e) =>
            assert(e.getMessage === "Vul een naam in,Vul een geldige postcode in (voorbeeld: 9999 BA)")
        }
      case None =>
        fail()
    }
  }

  "UserEndpoint mapping to new output" should "transform to BadRequest if result is throwable" in {
    val futureResult = UserEndpoint.mapRegisterResultToOutput(Future.apply(Left(new Exception("exception message"))))

    val result = Await.result(futureResult)

    assert(result.status == Status.BadRequest)

    Try(result.value) match {
      case Failure(exception) => assert(exception.getMessage == "Het opgegeven e-mailadres is al in gebruik")
      case Success(_) => fail("Should have gotten an exception")
    }
  }

  "UserEndpoint mapping to new output" should "transform to Created if result is throwable" in {
    val futureResult = UserEndpoint.mapRegisterResultToOutput(Future.apply(Right(())))

    val result = Await.result(futureResult)

    assert(result.status == Status.Created)
  }

  "UserEndpoint" should "get the user from the request" in {
    val input = Input.get("/user?access_token=6BBB0DA1891646E58EB3E6A63AF3A6FC3C8EB5A0D44824CBA581D2E14A0450CF")
    val endpoint = UserEndpoint.getUser(FakeOAuth2AuthenticationHandler)

    val result = endpoint(input).awaitOutputUnsafe().get

    assert(
      result.value == User(
        PersonalData("email@hotmail.com", "full name", "address", "1102 XL", "Amsterdam"),
        "password"
      )
    )
  }
}
