package com.petsupplies.interpreters

import java.time.Instant
import java.util.Date

import cats.{Id, ~>}
import com.petsupplies.authenticationservice.dsl.AuthAction
import com.petsupplies.authenticationservice.dsl.AuthAction._
import com.petsupplies.authenticationservice.interpreter.AuthInterpreter
import com.petsupplies.authenticationservice.types.AccessToken
import com.petsupplies.authenticationservice.types.User.User
import AuthInterpreterTestData._
import com.petsupplies.authenticationservice.types.PersonalData.PersonalData

import scala.concurrent.Future

object FutureAuthInterpreter {
  def interpret: AuthAction ~> Future =
    new (AuthAction ~> Future) {
      def apply[A](fa: AuthAction[A]): Future[A] =
        fa match {
          case Register(user) =>
            Future.successful(Right(()))
          case FindUserAndClientIdByAccessToken(accessToken) =>
            accessToken match {
              case `dummyCorrectAccessToken` =>
                Future.successful {
                  Some(
                    (dummyUser, dummyCorrectClientId)
                  )
                }
              case _ =>
                Future.successful {
                  None
                }
            }
          case FindAccessToken(stringToken) =>
            stringToken match {

              case `dummyCorrectAccessToken` =>
                Future.successful {
                  Some(
                    AccessToken(dummyUser, dummyCorrectClientId, stringToken, dummyInstant)
                  )
                }

              case _ =>
                Future.successful {
                  None
                }
            }
          case ValidateClient(client) =>
            client.clientId match {
              case `dummyCorrectClientId` =>
                Future.successful(true)
              case _ =>
                Future.successful(false)
            }
          case FindUser(username, password) =>
            (username, password) match {
              case ("email@hotmail.com", "5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8") =>
                Future.successful(Some(dummyUser))
              case _ =>
                Future.successful(None)
            }
          case CreateAccessToken(user, clientId) =>
            Future.successful(AccessToken(user, clientId, dummyCorrectAccessToken, dummyInstant))
          case RegisterAccessToken(_) =>
            Future.successful(())
          case FindAccessTokenByUserAndClientId(user, clientId) =>
            (user.personalData.email, clientId) match {
              case ("email@hotmail.com", `dummyCorrectClientId`) =>
                Future.successful(
                  Some(
                    AccessToken(user, clientId, dummyCorrectAccessToken, dummyInstant)
                  )
                )
              case (_, _) =>
                Future.successful(None)
            }
          case Logout(email, clientId) =>
            Future.successful(())
        }
    }
}

object PureIdAuthInterpreter {
  def interpret: AuthAction ~> Id =
    new (AuthAction ~> Id) {
      def apply[A](fa: AuthAction[A]): Id[A] =
        fa match {
          case Register(user) =>
            Right[Throwable, Unit](())

          case FindUserAndClientIdByAccessToken(accessToken) =>
            accessToken match {
              case `dummyCorrectAccessToken` =>
                Some(
                  (dummyUser, dummyCorrectClientId)
                )
              case _ => None
            }
          case FindAccessToken(stringToken) =>
            stringToken match {

              case `dummyCorrectAccessToken` =>
                Some(
                  AccessToken(dummyUser, dummyCorrectClientId, stringToken, dummyInstant)
                )

              case _ =>
                None
            }
          case ValidateClient(client) =>
            client.clientId match {
              case `dummyCorrectClientId` =>
                true
              case _ =>
                false
            }
          case FindUser(username, password) =>
            (username, password) match {
              case ("email@hotmail.com", "5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8") =>
                Some(dummyUser)
              case _ =>
                None
            }
          case CreateAccessToken(user, clientId) =>
            AccessToken(user, clientId, dummyCorrectAccessToken, dummyInstant)
          case RegisterAccessToken(_) =>
            ()
          case FindAccessTokenByUserAndClientId(user, clientId) =>
            (user.personalData.email, clientId) match {
              case ("email@hotmail.com", `dummyCorrectClientId`) =>
                Some(
                  AccessToken(user, clientId, dummyCorrectAccessToken, dummyInstant)
                )
              case (_, _) =>
                None
            }
          case Logout(email, clientId) =>
            ()
        }
    }
}

object AuthInterpreterTestData {
  val dummyCorrectAccessToken: String = "421c76d77563afa1914846b010bd164f395bd34c2102e5e99e0cb9cf173c1d87"
  val dummyUser: User = User(
    PersonalData(
      "email@hotmail.com",
      "full name",
      "address 12",
      "1192 XL",
      "residence"
    ),
    "5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8",
  )
  val dummyCorrectClientId: String = "dbc94d04-fa51-4db6-9170-f45194f10ab5"
  val dummyInstant: Instant = new Date(1501591800).toInstant
}
