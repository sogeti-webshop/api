package com.petsupplies.interpreters

import cats.{Id, ~>}
import com.petsupplies.productservice.dsl.ProductAction
import com.petsupplies.productservice.dsl.ProductAction.{AllProducts, GetProduct}
import com.petsupplies.productservice.types.Product.Product

object PureProductInterpreter {
  def pureInterpret: ProductAction ~> Id  =
    new (ProductAction ~> Id) {
      def apply[A](fa: ProductAction[A]): Id[A] =
        fa match {
          case AllProducts() =>
            products
          case GetProduct(id) =>
            products.find(product => product.id == id)
        }
    }

  val products: List[Product] = List(
    Product(1, "Product1", "Description", 29.95),
    Product(2, "Product2", "Description", 39.95)
  )
}
