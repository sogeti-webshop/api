package com.petsupplies.implicits.decoders

import com.petsupplies.generators.UserGen._
import org.scalacheck.Prop.forAll
import org.scalacheck.Properties
import org.scalatest.Matchers

object UserDecoderProp extends Properties("User") with Matchers {
  property("Valid JsonObject converts to user instance") = forAll(validUserAsJsonGenerator) { jsonUser =>
    // Parse the objects out of the json object and validate whether done properly
    UserDecoder.userDecoder.decodeJson(jsonUser) match {
      case Right(user) =>
        jsonUser.asObject.get.apply("personalData").get.asObject.get.apply("email").get.asString.get == user.personalData.email &&
        jsonUser.asObject.get.apply("password").get.asString.get == user.password &&
        jsonUser.asObject.get.apply("personalData").get.asObject.get.apply("fullName").get.asString.get == user.personalData.fullName &&
        jsonUser.asObject.get.apply("personalData").get.asObject.get.apply("address").get.asString.get == user.personalData.address &&
        jsonUser.asObject.get.apply("personalData").get.asObject.get.apply("postalCode").get.asString.get == user.personalData.postalCode &&
        jsonUser.asObject.get.apply("personalData").get.asObject.get.apply("residence").get.asString.get == user.personalData.residence
      case Left(_) => fail()
    }
  }

  property("Valid JsonObject converts to personalData instance") = forAll(validPersonalDataAsJsonGenerator) { jsonPersonalData =>
    // Parse the objects out of the json object and validate whether done properly
    UserDecoder.personalDataDecoder.decodeJson(jsonPersonalData) match {
      case Right(personalData) =>
        jsonPersonalData.asObject.get.apply("email").get.asString.get == personalData.email &&
            jsonPersonalData.asObject.get.apply("fullName").get.asString.get == personalData.fullName &&
            jsonPersonalData.asObject.get.apply("address").get.asString.get == personalData.address &&
            jsonPersonalData.asObject.get.apply("postalCode").get.asString.get == personalData.postalCode &&
            jsonPersonalData.asObject.get.apply("residence").get.asString.get == personalData.residence
      case Left(_) => fail()
    }
  }
}

