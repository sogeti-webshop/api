package com.petsupplies.implicits.decoders

import io.circe.{Json, JsonObject}
import org.scalatest.{FlatSpec, Matchers}

class UserDecoderSpec extends FlatSpec with Matchers {
  "UserDecoder" should "fail if provided json is not an object" in {
    UserDecoder.userDecoder.decodeJson(Json.fromString("not an object")) match {
      case Right(_) => fail()
      case Left(exception) => assert(exception.message == "Attempt to decode value on failed cursor")
    }
  }

  "UserDecoder" should "fail if one expected property of object is not present" in {
    val jsonUserWithoutPassword = Json.fromJsonObject(JsonObject.empty
        .add("email", Json.fromString("email@address.com"))
        .add("fullName", Json.fromString("full name"))
        .add("address", Json.fromString("address 17"))
        .add("postalCode", Json.fromString("1187XL"))
        .add("residence", Json.fromString("residence"))
    )

    UserDecoder.userDecoder.decodeJson(jsonUserWithoutPassword) match {
      case Right(_) => fail()
      case Left(exception) => assert(exception.message == "Attempt to decode value on failed cursor")
    }
  }
}
