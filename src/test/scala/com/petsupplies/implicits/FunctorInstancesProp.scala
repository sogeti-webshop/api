package com.petsupplies.implicits

import org.scalacheck.Properties
import org.scalacheck.Prop.forAll
import com.petsupplies.implicits.FunctorInstances.twitterFutureFunctor
import com.twitter.util.{Future, Await}


object FunctorInstancesProp extends Properties("FunctorInstances") {
  property("TwitterFuture Functor identity law") = forAll { number: Int =>
    val futureResult = twitterFutureFunctor.map(Future.apply(number))(x => x)

    Await.result(futureResult) == number
  }

  // map (f andThen g) == map(f).map(g)
  property("TwitterFuture Functor composition law") = forAll { number: Int =>
    def plus1(x: Int) = x + 1
    def plus3(x: Int) = x + 3

    val futureResult1 = twitterFutureFunctor.map(Future.apply(number))(plus1 _ andThen plus3)
    val futureResult2 = twitterFutureFunctor.map(Future.apply(number))(plus1).map(plus3)

    val result1 = Await.result(futureResult1)
    val result2 = Await.result(futureResult2)

    result1 == result2
  }
}
