package com.petsupplies.implicits.extensions

import com.twitter.util.{Try, Await => TwitterAwait, Future => TwitterFuture}
import com.petsupplies.implicits.extensions.FutureConverter._
import org.scalacheck.Properties
import org.scalacheck.Prop.forAll
import org.scalacheck.Gen.alphaNumStr

import scala.concurrent.{Future => ScalaFuture}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await => ScalaAwait}
import scala.concurrent.duration._
import scala.util.{Failure, Success}


object FutureConverterProp extends Properties("FutureConversion") {
  property("Twitter Future should convert to Scala Future") = forAll { number: Int =>
    val twitterFuture = TwitterFuture.apply(number)

    val scalaFuture = twitterFuture.asScala

    TwitterAwait.result(twitterFuture) == ScalaAwait.result(scalaFuture, Duration.Inf)
  }

  property("Failed Twitter Future should convert to failed Scala future") = forAll(alphaNumStr) { message: String =>
    val exception = new Exception(message)

    val twitterFuture = TwitterFuture.exception(exception)

    val scalaFuture = twitterFuture.asScala

    scalaFuture.value match {
      case Some(Success(_)) => false
      case Some(Failure(e)) =>
        e.getMessage == message
      case None => false
    }
  }

  property("Scala Future should convert to Twitter Future") = forAll { number: Int =>
    val scalaFuture = ScalaFuture.apply(number)

    val twitterFuture = scalaFuture.asTwitter

    TwitterAwait.result(twitterFuture) == ScalaAwait.result(scalaFuture, Duration.Inf)
  }

  property("Failed Scala Future should convert to Twitter Future") = forAll(alphaNumStr) { message: String =>
    val exception = new Exception(message)

    val scalaFuture = ScalaFuture.failed[Int](exception)

    val twitterFuture = scalaFuture.asTwitter

    val tryException = Try(TwitterAwait.result(twitterFuture))

    tryException.asScala match {
      case Success(_) => false
      case Failure(e) =>
        e.getMessage == message
    }
  }
}
