package com.petsupplies.implicits.encoders

import org.scalacheck.Properties
import org.scalacheck.Prop.forAll
import ProductEncoder.productEncoder
import com.petsupplies.generators.ProductGen.productGenerator

object ProductEncoderProp extends Properties("ProductEncoder") {

  property("product converts to json object") = forAll(productGenerator) { product =>
    // Parse the objects out of the json object and validate whether done properly
    product.id == productEncoder(product).asObject.get.apply("id").get.asNumber.get.toInt.get &&
    product.name == productEncoder(product).asObject.get.apply("name").get.asString.get &&
    product.description == productEncoder(product).asObject.get.apply("description").get.asString.get &&
    product.price == productEncoder(product).asObject.get.apply("price").get.asNumber.get.toDouble
  }
}
