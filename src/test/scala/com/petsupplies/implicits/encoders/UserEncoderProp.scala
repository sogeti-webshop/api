package com.petsupplies.implicits.encoders

import org.scalacheck.Properties
import org.scalacheck.Prop.forAll
import com.petsupplies.generators.UserGen.validUserGen
import com.petsupplies.implicits.encoders.UserEncoder.{userEncoder, userWithPasswordEncoder}

object UserEncoderProp extends Properties("UserEncoder") {
  property("user converts to json object") = forAll(validUserGen) { user => {
    user.personalData.email == userEncoder(user).asObject.get.apply("personalData").get.asObject.get.apply("email").get.asString.get &&
    user.personalData.fullName == userEncoder(user).asObject.get.apply("personalData").get.asObject.get.apply("fullName").get.asString.get
    user.personalData.postalCode == userEncoder(user).asObject.get.apply("personalData").get.asObject.get.apply("postalCode").get.asString.get &&
    user.personalData.address == userEncoder(user).asObject.get.apply("personalData").get.asObject.get.apply("address").get.asString.get &&
    user.personalData.residence == userEncoder(user).asObject.get.apply("personalData").get.asObject.get.apply("residence").get.asString.get &&
    userEncoder(user).asObject.get.apply("password").isEmpty //password may not be present when encoding to json
  }}

  property("user converts with password to json object") = forAll(validUserGen) { user => {
    user.personalData.email == userWithPasswordEncoder(user).asObject.get.apply("personalData").get.asObject.get.apply("email").get.asString.get &&
    user.password == userWithPasswordEncoder(user).asObject.get.apply("password").get.asString.get &&
    user.personalData.fullName == userWithPasswordEncoder(user).asObject.get.apply("personalData").get.asObject.get.apply("fullName").get.asString.get &&
    user.personalData.postalCode == userWithPasswordEncoder(user).asObject.get.apply("personalData").get.asObject.get.apply("postalCode").get.asString.get &&
    user.personalData.address == userWithPasswordEncoder(user).asObject.get.apply("personalData").get.asObject.get.apply("address").get.asString.get &&
    user.personalData.residence == userWithPasswordEncoder(user).asObject.get.apply("personalData").get.asObject.get.apply("residence").get.asString.get
  }}
}
