package com.petsupplies.implicits.encoders

import org.scalacheck.Properties
import org.scalacheck.Prop.forAll
import org.scalacheck.Gen.alphaNumStr

import scalaoauth2.provider.{InvalidRequest, OAuthError}

object ExceptionEncoderProp extends Properties("ExceptionEncoder") {
  property("encodes to object with 1 key named message") = forAll(alphaNumStr) {
    message: String => {
      val exception = new Exception(message)
      val encodedException = ExceptionEncoder.exceptionEncoder.apply(exception)

      encodedException.asObject.get.apply("message").get.asString.get == exception.getMessage &&
      encodedException.asObject.get.fields.length == 1
    }
  }

  property("encodes to 1 key object named message when encoding OAuthError") = forAll(alphaNumStr) {
    message: String => {
      val exception = new InvalidRequest(message)
      val encodedException = ExceptionEncoder.exceptionEncoder.apply(exception)

      encodedException.asObject.get.apply("message").get.asString.get == exception.description &&
          encodedException.asObject.get.fields.length == 1
    }
  }
}
