package com.petsupplies.generators

import com.petsupplies.productservice.types.Product.Product
import org.scalacheck.{Arbitrary, Gen}

object ProductGen {
  val productGenerator: Gen[Product] = for {
    id <- Gen.choose(0, Int.MaxValue)
    name <- Arbitrary.arbitrary[String]
    description <- Arbitrary.arbitrary[String]
    price <- Gen.choose(0.0, 1000000.0)
  } yield Product(id, name, description, price)
}
