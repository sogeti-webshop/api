package com.petsupplies.generators

import com.petsupplies.authenticationservice.types.User.User
import com.petsupplies.authenticationservice.types.PersonalData.PersonalData
import io.circe.{Json, JsonObject}
import org.scalacheck.Gen

object UserGen {
  val validEmailGen: Gen[String] = for {
    before <- Gen.alphaNumStr if before.length > 0
    middle <- Gen.alphaNumStr if middle.length > 0
    after <- Gen.alphaStr if after.length >= 2
  } yield before ++ "@" ++ middle ++ "." ++ after

  val validPasswordGen: Gen[String] = for {
    password <- Gen.alphaStr if password.length >= 6
  } yield password

  val validFullNameGen: Gen[String] = for {
    fullName <- Gen.alphaStr if fullName.length > 0
  } yield fullName

  val validAddressGen: Gen[String] = for {
    address <- Gen.alphaStr if address.length > 0
  } yield address

  val validPostalCodeGen: Gen[String] = for {
    first <- Gen.choose(1, 9)
    second <- Gen.choose(0, 9)
    third <- Gen.choose(0, 9)
    forth <- Gen.choose(0, 9)

    maybeSpace <- Gen.oneOf("", " ")

    firstChar <- Gen.alphaChar
    secondChar <- Gen.alphaChar if !(firstChar == 's' && (secondChar == 'a' || secondChar == 'd' || secondChar == 's'))
  } yield List(first, second, third, forth).mkString ++ maybeSpace ++ List(firstChar, secondChar).mkString

  val validResidenceGen: Gen[String] = for {
    residence <- Gen.alphaNumStr if residence.length > 0
  } yield residence

  val validPersonalDataGen: Gen[PersonalData] = for {
    email <- validEmailGen
    fullName <- validFullNameGen
    address <- validAddressGen
    postalCode <- validPostalCodeGen
    residence <- validResidenceGen
  } yield PersonalData(email, fullName, address, postalCode, residence)

  val validUserGen: Gen[User] = for {
    personalData <- validPersonalDataGen
    password <- validPasswordGen
  } yield User(personalData, password)

  val validPersonalDataAsJsonGenerator: Gen[Json] = for {
    personalData <- validPersonalDataGen
  } yield Json.fromJsonObject(JsonObject.empty
      .add("email", Json.fromString(personalData.email))
      .add("fullName", Json.fromString(personalData.fullName))
      .add("address", Json.fromString(personalData.address))
      .add("postalCode", Json.fromString(personalData.postalCode))
      .add("residence", Json.fromString(personalData.residence))
  )

  val validUserAsJsonGenerator: Gen[Json] = for {
    password <- validPasswordGen
    personalData <- validPersonalDataAsJsonGenerator
  } yield Json.fromJsonObject(JsonObject.empty
      .add("personalData", personalData)
      .add("password", Json.fromString(password)),
  )

  val invalidEmailGen: Gen[String] = Gen.alphaNumStr

  val invalidPasswordGen: Gen[String] = for {
    randomNumber <- Gen.choose(0, 5)
    password <- Gen.listOfN(randomNumber, Gen.alphaNumChar)
  } yield password.mkString

  val invalidPostalCode: Gen[String] = Gen.alphaNumStr
}
