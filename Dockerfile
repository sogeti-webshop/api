FROM debian:latest

RUN apt-get --yes update && apt-get --yes upgrade && apt-get --yes install default-jre

COPY ./target/scala-2.12/Api-assembly-0.1.0.jar /opt/

CMD ["java", "-jar", "/opt/Api-assembly-0.1.0.jar"]
