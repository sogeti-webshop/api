name := "Api"

version := "0.1.0"

scalaVersion := "2.12.2"

val finchVersion = "0.15.0"
val circeVersion = "0.8.0"

libraryDependencies ++= Seq(
  "com.github.finagle" %% "finch-core" % finchVersion,
  "com.github.finagle" %% "finch-circe" % finchVersion,
  "com.nulab-inc" %% "scala-oauth2-core" % "1.3.0",
  "io.circe" %% "circe-core" % circeVersion,
  "io.circe" %% "circe-generic" % circeVersion,
  "org.typelevel" %% "cats" % "0.9.0",
  "org.typelevel" %% "cats-effect" % "0.3",
  "org.tpolecat" %% "doobie-core-cats" % "0.4.1",
  "org.tpolecat" %% "doobie-scalatest-cats" % "0.4.1",
  "mysql" % "mysql-connector-java" % "5.1.35",
  "com.roundeights" %% "hasher" % "1.2.0",
  "com.petsupplies" %% "productservice" % "0.2.0",
  "com.petsupplies" %% "authenticationservice" % "0.2.0",
  "com.pepegar" %% "hammock-core" % "0.6.3",
  "org.scalatest" %% "scalatest" % "3.0.1" % "test",
  "org.scalacheck" %% "scalacheck" % "1.13.4" % "test"
)

sonarProperties ++= Map(
  "sonar.host.url" -> "http://localhost:9000",
  "sonar.scoverage.reportPath" -> "target/scala-2.12/scoverage-report/scoverage.xml"
)

assemblyMergeStrategy in assembly := {
 case PathList("META-INF", xs @ _*) => MergeStrategy.discard
 case x => MergeStrategy.first
}

lazy val commonSettings = Seq(
  version := "0.1.0",
  organization := "com.petsupplies",
  scalaVersion := "2.12.2",
  test in assembly := {}
)

lazy val app = (project in file(".")).
  settings(commonSettings: _*).
  settings(mainClass in assembly := Some("com.petsupplies.Main"))

enablePlugins(SonarRunnerPlugin)
