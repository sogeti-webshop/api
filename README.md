# API

This project contains the primary endpoint. It can be seen as a monolithic back-end which exposes endpoints everything product and authentication related.

## Development

To contribute to this application, make sure that Scala V2.12.2 and SBT v0.13.6 are installed.

To run test

```
sbt test
```

To compile the code

```
sbt compile
```

To run the application (which will expose the API on port 8081)

```
sbt run
```

running this application requires that a host called mysql can be resolved. This can be done on unix systems by editing the /etc/hosts. The following record can be added:
```
127.0.0.1       mysql
```

This will redirect the mysql to localhost. It assumes that mysql runs on it's default port (3306)

Also, since this projects makes no use of environment variables, the mysql need to have a fixed user with username "root" and password "". Also, the database is called petsupplies.

The MySql instance can be anything. You can run it locally on your machine, have it in Vagrant box, SSH to somewhere else or have it running in a container.

## Sonar

To verify testcoverage, you can publish to sonar by running the `publish-to-sonar.sh` script.

## CI/CD

Since there is no access to servers, the CI is simulated with a shellscript that will dockerize the application whenever triggered (manually for now).

```
sh ./build-and-publish.sh
```

This will run tests, compiles the code and assembles all it's dependencies. Next up it will create a new docker image and publish this to the registry.

## TechDebt

The hardcoded database, credentials and url is by far from ideal. The reason to make them fixed for now is that the application will not be deployed and it reduces complexity of this service, since getting the environment variables are a side-effect which may not just be ran somewhere in the code. Since this is one of the first applications developed using purely functional programming, I decided to keep it simple and add it later if time is left.
