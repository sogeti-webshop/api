#!/bin/bash/

sbt clean compile test assembly exit && docker build -t registry.gitlab.com/sogeti-webshop/api . && docker push registry.gitlab.com/sogeti-webshop/api
